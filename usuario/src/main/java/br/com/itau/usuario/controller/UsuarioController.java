package br.com.itau.usuario.controller;

import br.com.itau.usuario.DTO.UsuarioEntrada;
import br.com.itau.usuario.model.Usuario;
import br.com.itau.usuario.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController
{
    @Autowired
    UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @NewSpan(name = "API Criar Usuário")
    public Usuario criar(@RequestBody @Valid UsuarioEntrada usuario)
    {
        System.out.println(System.currentTimeMillis() + " - Tentaram criar um usuário: " + usuario.getNome() + " CEP: " + usuario.getCep());
        return  usuarioService.criar(usuario);
    }
}
