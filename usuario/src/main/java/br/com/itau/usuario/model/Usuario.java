package br.com.itau.usuario.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Usuario
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message =  "Nome não pode ser nulo")
    @NotBlank(message =  "Nome não pode ser branco")
    private String nome;

    @NotNull(message =  "CEP não pode ser nulo")
    @NotBlank(message =  "CEP não pode ser branco")
    private String cep;

    @NotNull(message =  "Logradouro não pode ser nulo")
    @NotBlank(message =  "Logradouro não pode ser branco")
    private String logradouro;

    @NotNull(message =  "Número não pode ser nulo")
    private Integer numero;

    private String complemento;

    @NotNull(message =  "Bairro não pode ser nulo")
    @NotBlank(message =  "Bairro não pode ser branco")
    private String bairro;

    @NotNull(message =  "Localidade não pode ser nulo")
    @NotBlank(message =  "Localidade não pode ser branco")
    private String localidade;

    @NotNull(message =  "UF não pode ser nulo")
    @NotBlank(message =  "UF não pode ser branco")
    private String uf;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }


    public Usuario() {
    }
}
