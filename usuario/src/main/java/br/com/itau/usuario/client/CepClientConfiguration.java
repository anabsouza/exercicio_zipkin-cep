package br.com.itau.usuario.client;

import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CepClientConfiguration
{
    @Bean
    public ErrorDecoder getClienteClientDecoder()
    {
        return new CepCodeDecoder();
    }

    @Bean
    public Feign.Builder builder()
    {
        FeignDecorators decorators =  FeignDecorators.builder()
                .withFallback(new CepClientFallback(), feign.RetryableException.class)
                .withFallbackFactory(CepClientLoadBalancerFallback::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
