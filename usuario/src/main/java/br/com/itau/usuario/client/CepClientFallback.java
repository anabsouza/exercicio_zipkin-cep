package br.com.itau.usuario.client;

import br.com.itau.usuario.exception.APICepNaoDisponivelException;

public class CepClientFallback implements CepClient {
    @Override
    public Cep buscarCep(String cep) {
        throw new APICepNaoDisponivelException();
    }
}