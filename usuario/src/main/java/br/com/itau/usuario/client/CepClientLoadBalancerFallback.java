package br.com.itau.usuario.client;

import br.com.itau.usuario.exception.APICepNaoDisponivelException;
import com.netflix.client.ClientException;

public class CepClientLoadBalancerFallback implements CepClient
{
    private Exception ex;

    public CepClientLoadBalancerFallback(Exception ex)
    {
        this.ex = ex;
    }

    @Override
    public Cep buscarCep(String cep) {
        if(ex.getCause() instanceof ClientException)
        {
            throw new APICepNaoDisponivelException();
        }
        throw (RuntimeException) ex;
    }
}