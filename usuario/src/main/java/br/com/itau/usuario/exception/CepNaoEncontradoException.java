package br.com.itau.usuario.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.NOT_FOUND, reason = "CEP Inexistente")
public class CepNaoEncontradoException extends RuntimeException  {
}