package br.com.itau.usuario.service;

import br.com.itau.usuario.DTO.UsuarioEntrada;
import br.com.itau.usuario.client.Cep;
import br.com.itau.usuario.client.CepClient;
import br.com.itau.usuario.model.Usuario;
import br.com.itau.usuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService
{
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private CepClient cepClient;

    @NewSpan(name = "Serviço Criar Usuário")
    public Usuario criar(UsuarioEntrada usuarioEntrada)
    {
        usuarioEntrada.setCep(usuarioEntrada.getCep().replace("-", ""));

        return  usuarioRepository.save(montarUsuario(usuarioEntrada, buscarCEP(usuarioEntrada.getCep())));
    }

    private Usuario montarUsuario(UsuarioEntrada usuarioEntrada, Cep cep)
    {
        Usuario usuario = new Usuario();
        usuario.setCep(usuarioEntrada.getCep());
        usuario.setNome(usuarioEntrada.getNome());
        usuario.setComplemento(usuarioEntrada.getComplemento());
        usuario.setNumero(usuarioEntrada.getNumero());
        usuario.setBairro(cep.getBairro());
        usuario.setLocalidade(cep.getLocalidade());
        usuario.setLogradouro(cep.getLogradouro());
        usuario.setUf(cep.getUf());

        return usuario;
    }

    private Cep buscarCEP(@SpanTag("cep") String cep)
    {
        return cepClient.buscarCep(cep);
    }
}
