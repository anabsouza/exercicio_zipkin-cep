package br.com.itau.cep.controller;

import br.com.itau.cep.model.Cep;
import br.com.itau.cep.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cep")
public class CepController
{
    @Autowired
    private CepService cepService;

    @GetMapping("/{cep}")
    @NewSpan(name = "API CEP")
    public Cep create(@SpanTag("cep") @PathVariable String cep) {
        return cepService.getByCep(cep);
    }
}
